package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/konfka/userservice/pkg/infrastructure/component"
	"gitlab.com/konfka/userservice/pkg/infrastructure/errors"
	"gitlab.com/konfka/userservice/pkg/infrastructure/handlers"
	"gitlab.com/konfka/userservice/pkg/infrastructure/responder"
	"gitlab.com/konfka/userservice/pkg/modules/user/service"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving parser error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	u.Profile(w, r)
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req ChangePasswordRequest
	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out := u.service.ChangePassword(r.Context(), service.ChangePasswordIn{
		UserID:      claims.ID,
		OldPassword: req.OldPassword,
		NewPassword: req.NewPassword,
	})

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Message:   "change password error",
		})
		return
	}

	u.OutputJSON(w, ChangePasswordResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Message:   "success change password",
	})

}
