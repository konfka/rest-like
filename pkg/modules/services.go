package modules

import (
	"gitlab.com/konfka/userservice/internal/storages"
	"gitlab.com/konfka/userservice/pkg/infrastructure/component"
	service2 "gitlab.com/konfka/userservice/pkg/modules/auth/service"
	"gitlab.com/konfka/userservice/pkg/modules/user/service"
)

type Services struct {
	User service.Userer
	Auth service2.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := service.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: service2.NewAuth(userService, storages.Verify, components),
	}
}
