package storages

import (
	"gitlab.com/konfka/userservice/internal/db/adapter"
	"gitlab.com/konfka/userservice/pkg/infrastructure/cache"
	storage2 "gitlab.com/konfka/userservice/pkg/modules/auth/storage"
	"gitlab.com/konfka/userservice/pkg/modules/user/storage"
)

type Storages struct {
	User   storage.Userer
	Verify storage2.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   storage.NewUserStorage(sqlAdapter, cache),
		Verify: storage2.NewEmailVerify(sqlAdapter),
	}
}
