package docs

import (
	ucontroller "gitlab.com/konfka/userservice/pkg/modules/user/controller"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
// 200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in: body
	Body ucontroller.ProfileResponse
}

// swagger:route POST /api/1/user/profile/chpass user chpassRequest
// Смена пароля.
// security:
//   - Bearer: []
// responses:
// 200: chpassResponse

// swagger:parameters chpassRequest
type chpassRequest struct {
	// in: body
	Body ucontroller.ChangePasswordRequest
}

// swagger:response chpassResponse
type chpassResponse struct {
	// in: body
	Body ucontroller.ChangePasswordResponse
}
